const SHARED_MODULES = [
  'ms-web-shared',
  'ms-api-shared',
  'ms-isomorphic-shared',
  'ms-config',
  'ms-devices',
];
const plugins = [
  'import',
  'node',
  'promise',
  'react',
  'react-hooks',
  'jsx-a11y',
  '@typescript-eslint',
];
const commonConfigs = ['standard'];
const backendConfigs = [...commonConfigs];
const frontendConfigs = [...commonConfigs];
const commonRecommendedConfigs = ['promise/recommended', 'import/errors'];
const backendRecommendedConfigs = [
  'node/recommended',
  ...commonRecommendedConfigs,
];
const frontendRecommendedConfigs = [
  'react/recommended',
  'react-hooks/recommended',
  ...commonRecommendedConfigs,
];

// check if rule is already part of a plugin
function isPluginRule(ruleName) {
  for (const pluginName of plugins) {
    if (ruleName.includes(`${pluginName}/`)) {
      return true;
    }
  }
  return false;
}

const requirePlugin = (pluginName) => {
  const fullPluginName = pluginName.startsWith('@')
    ? `${pluginName}/eslint-plugin`
    : pluginName.startsWith('eslint-')
    ? pluginName
    : `eslint-plugin-${pluginName}`;
  return require(fullPluginName);
};

// prefix plugin rules with name of plugin
const pluginRules = {};
for (const pluginName of plugins) {
  const plugin = requirePlugin(pluginName);

  Object.keys(plugin.rules).forEach((ruleName) => {
    pluginRules[`${pluginName}/${ruleName}`] = plugin.rules[ruleName];
  });
}

function getConfigRules(configs) {
  // prefix config rules with name of config
  const configRules = {};
  for (const configName of configs) {
    const pluginName = configName.startsWith('eslint-')
      ? configName
      : `eslint-config-${configName}`;
    // eslint-disable-next-line self/@typescript-eslint/no-var-requires
    const config = require(pluginName);

    Object.keys(config.rules).forEach((ruleName) => {
      if (isPluginRule(ruleName)) {
        configRules[`@medicspot/${ruleName}`] = config.rules[ruleName];
      } else {
        configRules[ruleName] = config.rules[ruleName];
      }
    });
  }

  return configRules;
}

function getPluginRecommendedRules(pluginRecommendedConfigs) {
  // plugins often have recommended rules, we want to use them but the
  // rules are prefixed now so we must update the recommended rules names
  // to reflect that
  const pluginRecommendedRules = {};
  for (const recName of pluginRecommendedConfigs) {
    const [pluginName, configName] = recName.split('/');
    const plugin = requirePlugin(pluginName);
    const recommendedRules = plugin.configs
      ? plugin.configs[configName].rules
      : plugin.rules;

    Object.keys(recommendedRules).forEach((ruleName) => {
      // only prefix plugin rules, not eslint baserules
      if (ruleName.startsWith(pluginName)) {
        pluginRecommendedRules[`@medicspot/${ruleName}`] =
          recommendedRules[ruleName];
      } else {
        pluginRecommendedRules[ruleName] = recommendedRules[ruleName];
      }
    });
  }

  return pluginRecommendedRules;
}

const commonRules = {
  'prefer-template': ['error'],
  '@medicspot/import/first': ['error'], // typescript overlap
  '@medicspot/import/namespace': ['error', { allowComputed: true }], // typescript overlap
  '@medicspot/import/order': [
    'error',
    {
      alphabetize: {
        order: 'asc',
        caseInsensitive: true,
      },
      'newlines-between': 'never',
      groups: ['builtin', 'external', 'unknown', 'parent', 'sibling', 'index'],
      pathGroups: SHARED_MODULES.map((path) => ({
        pattern: `${path}/**`,
        group: 'unknown',
      })),
      pathGroupsExcludedImportTypes: ['builtin'],
    },
  ],
};

const commonStyleRules = {
  'brace-style': ['error', 'stroustrup'],
  indent: ['error', 2, { MemberExpression: 0 }],
  semi: ['error', 'always'],
  'object-curly-spacing': ['error', 'never'],
  'quote-props': ['error', 'consistent-as-needed'],
};

const commonSettings = {
  // package resolution due to custom node-path and mounting modules through
  // docker volumes
  'import/resolver': {
    node: {
      paths: [
        '.', // NODE_PATH is set to this in all the apps
      ],
    },
  },
  'import/ignore': [
    '.test.[tj]sx?$', // can't handle mocked dependencies in tests
  ],
};

const pluginOverrides = [
  {
    files: ['**/test/index.[tj]s'],
    env: {
      mocha: true,
    },
    plugins: ['mocha'],
    extends: ['plugin:mocha/recommended'],
  },
  {
    files: ['*.(test|spec).[tj]sx?'],
    plugins: ['jest'],
    env: {
      'jest/globals': true,
    },
    extends: ['plugin:jest/recommended', 'plugin:jest/style'],
  },
];

const commonParserOptions = {
  ecmaVersion: 2020,
  sourceType: 'module',
};

const jsxAccessibilityRules = {
  '@medicspot/jsx-a11y/accessible-emoji': 'warn',
  '@medicspot/jsx-a11y/alt-text': 'warn',
  '@medicspot/jsx-a11y/anchor-has-content': 'warn',
  '@medicspot/jsx-a11y/anchor-is-valid': [
    'warn',
    {
      aspects: ['noHref', 'invalidHref'],
    },
  ],
  '@medicspot/jsx-a11y/aria-activedescendant-has-tabindex': 'warn',
  '@medicspot/jsx-a11y/aria-props': 'warn',
  '@medicspot/jsx-a11y/aria-proptypes': 'warn',
  '@medicspot/jsx-a11y/aria-role': ['warn', { ignoreNonDOM: true }],
  '@medicspot/jsx-a11y/aria-unsupported-elements': 'warn',
  '@medicspot/jsx-a11y/heading-has-content': 'warn',
  '@medicspot/jsx-a11y/iframe-has-title': 'warn',
  '@medicspot/jsx-a11y/img-redundant-alt': 'warn',
  '@medicspot/jsx-a11y/no-access-key': 'warn',
  '@medicspot/jsx-a11y/no-distracting-elements': 'warn',
  '@medicspot/jsx-a11y/no-redundant-roles': 'warn',
  '@medicspot/jsx-a11y/role-has-required-aria-props': 'warn',
  '@medicspot/jsx-a11y/role-supports-aria-props': 'warn',
  '@medicspot/jsx-a11y/scope': 'warn',
};

const nodeRules = {
  '@medicspot/node/no-missing-require': [
    'error',
    {
      resolvePaths: ['.'],
      allowModules: SHARED_MODULES,
    },
  ],
  // we don't need this in most cases as services/apps aren't being published
  // should be enabled in packages though
  '@medicspot/node/no-unpublished-require': ['off'],
};

const backendConfig = {
  env: {
    es6: true,
    node: true,
  },
  parserOptions: {
    ...commonParserOptions,
  },
  plugins: ['@medicspot'],
  rules: {
    // if rules are coming from a plugin they need to be prefixed with
    // `@medicspot/...`
    ...getPluginRecommendedRules(backendRecommendedConfigs),
    ...getConfigRules(backendConfigs),
    // rules
    ...commonRules,
    ...nodeRules,
  },
  overrides: pluginOverrides,
};

const frontendConfig = {
  env: {
    browser: true,
    es6: true,
    jest: true,
  },
  globals: {
    // we use this for adding env vars to config at bundle time
    process: true,
  },
  parser: '@babel/eslint-parser',
  parserOptions: {
    ...commonParserOptions,
    ecmaFeatures: {
      jsx: true,
    },
  },
  plugins: ['@medicspot'],
  rules: {
    // if rules are coming from a plugin they need to be prefixed with
    // `@medicspot/...`
    ...getPluginRecommendedRules(frontendRecommendedConfigs),
    ...getConfigRules(frontendConfigs),
    // rule
    ...commonRules,
    // https://github.com/facebook/create-react-app/blob/master/packages/eslint-config-react-app/index.js
    ...jsxAccessibilityRules,

    '@medicspot/react/prop-types': ['off'],

    // package resolution due to custom node-path and mounting modules through
    // docker volumes
    '@medicspot/import/no-unresolved': [
      'error',
      {
        ignore: SHARED_MODULES,
      },
    ],
  },
  settings: {
    react: {
      version: 'detect',
    },
    ...commonSettings,
  },
};

module.exports = {
  configs: {
    backend: {
      ...backendConfig,
      rules: { ...backendConfig.rules, ...commonStyleRules },
    },
    frontend: {
      ...frontendConfig,
      rules: { ...frontendConfig.rules, ...commonStyleRules },
    },
    'backend-prettier': {
      ...backendConfig,
      parser: '@typescript-eslint/parser',
      rules: {
        ...backendConfig.rules,
        ...getPluginRecommendedRules([
          '@typescript-eslint/recommended',
          'eslint-config-prettier',
        ]),
      },
    },
    'frontend-prettier': {
      ...frontendConfig,
      parser: '@typescript-eslint/parser',
      rules: {
        ...frontendConfig.rules,
        ...getPluginRecommendedRules([
          '@typescript-eslint/recommended',
          'eslint-config-prettier',
        ]),
      },
    },
  },
  rules: pluginRules,
};
