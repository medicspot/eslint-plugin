# @medicspot/eslint-plugin

Our custom eslint plugin with four custom configs: `frontend`, `backend`, `frontent-prettier` and `backend-prettier`.

Frontend contains basic conventions and react plugin rules.  
Backend contains basic conventions and node rules.
The `-prettier` suffixed configs are the same as the others but remove the style-only rules, relying on prettier to perform the stylistic formatting and leaving eslint to catch code smells.
All contain jest, standard, import and promise plugins.

### Usage

You just need to install the plugin and then extend it in your config.

```
$ yarn add -D git+ssh://git@gitlab.com:medicspot/eslint-plugin
```

_.eslintrc_

```
{
extends: "plugin:@medicspot/eslint-plugin/<backend|frontend|backend-prettier|frontend-prettier>"
}
```

### Why a plugin not a config

Eslint allows you to easily create shareable configs with less complexity than this but then every consumer package has to add all the dependencies.  
By having this plugin setup, we can specify all the plugins in here and export the rules resulting in the consumer projects only having a single dependency on `@medicspot/eslint-plugin`.

### How does it work

For the most part it just takes a list of the plugins and configs we want to use and then re-exports them as part of our plugin. This means any non-core rules will be prefixed with `@medicspot`.  
For example to disable the `react/display-name` rule you would write`/* eslint-disable @medicspot/react/display-name */`.
